### 数据字典

------

user表

| 字段       | 类型           | 名称     | 备注                        |
| ---------- | -------------- | -------- | --------------------------- |
| id         | integer        | 主键     | 自增                        |
| password   | varchar（128） | 密码     |                             |
| username   | varchar（150） | 用户名   |                             |
| name       | varchar（128） | 姓名     |                             |
| student_no | varchar（20）  | 学号     |                             |
| mobile     | varchar（15）  | 手机号   |                             |
| type       | varchar（1）   | 类型     | 0：普通，1：管理：2超级管理 |
| unit_id    | varchar（36）  | 单位id   |                             |
| area       | varchar（128） | 所在地区 |                             |

c_unit表

| 字段      | 类型        | 名称   | 备注 |
| --------- | ----------- | ------ | ---- |
| id        | char(32)    | 主键   | 自增 |
| name      | varchar(20) | m名称  |      |
| grade     | varchar(1)  | 级别   |      |
| parent_id | char(32)    | 父级id |      |

daily_entry表

| 字段         | 类型        | 名称     | 备注         |
| ------------ | ----------- | -------- | ------------ |
| id           | char(32)    | 主键     | 自增         |
| if_cough     | varchar(1)  | 咳嗽     | 0：无；1：有 |
| breath_short | varchar(1)  | 呼吸     | 0：无；1：有 |
| temperature  | varchar(5)  | 温度     |              |
| other        | varchar(50) | 其他     |              |
| opdate       | datetime    | 录入时间 |              |
| unit_id      | char(32)    | 单位主键 |              |
| user_id      | integer     | 用户主键 |              |





### 接口文档

------

|      | user登录接口 |                                                 |
| ---- | ------------ | ----------------------------------------------- |
| 1    | 地址         | http://127.0.0.1:8000/health/api/v1/user/login/ |
| 2    | 请求方式     | POST                                            |
| 3    | 请求参数     | {"username": "8888", "password": "123123"}      |

|      | user维护接口 |                                                  |
| ---- | ------------ | ------------------------------------------------ |
| 1    | 地址         | <http://127.0.0.1:8000/health/api/v1/user/user/> |
| 2    | 请求方式     | POST、PUT、DELETE、GET                           |
| 3    | 请求参数     |                                                  |

|      | unit维护接口 |                                                 |
| ---- | ------------ | ----------------------------------------------- |
| 1    | 地址         | <http://127.0.0.1:8000/health/api/v1/app/unit/> |
| 2    | 请求方式     | POST、PUT、DELETE、GET                          |
| 3    | 请求参数     |                                                 |

|      | daily_entry维护接口 |                                                        |
| ---- | ------------------- | ------------------------------------------------------ |
| 1    | 地址                | <http://127.0.0.1:8000/health/api/v1/app/daily-entry/> |
| 2    | 请求方式            | POST、PUT、DELETE、GET                                 |
| 3    | 请求参数            |                                                        |

