import uuid

# from apps import auth_user
from django.db import models
from datetime import datetime
# Create your models here.


class Unit(models.Model):
    id = models.UUIDField(primary_key=True, auto_created=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=20, verbose_name="名称")
    grade = models.CharField(max_length = 1, null=True, blank=True, verbose_name="级别")
    parent_id = models.ForeignKey(to='self',null=True, blank=True, on_delete=models.CASCADE, verbose_name="父类id")

    class Meta:
        verbose_name = "单元"
        verbose_name_plural = verbose_name
        db_table = "c_unit"


# class Status(models.Model):
#     TYPE = (
#         ('0', True),
#         ('1', False)
#     )
#     id = models.UUIDField(primary_key=True, auto_created=True, default=uuid.uuid4, editable=False)
#     if_cough = models.CharField(choices=TYPE,null=True, max_length=1, verbose_name="是否咳嗽")
#     temperature = models.CharField(max_length = 5, null=True, blank=True, verbose_name="温度")
#     breath_short = models.CharField(choices=TYPE,null=True, max_length=1, verbose_name="是否呼吸困难")
#     other = models.CharField(max_length=50, verbose_name="名称")
#
#     class Meta:
#         verbose_name = "单元"
#         verbose_name_plural = verbose_name
#         db_table = "c_status"


class DailyEntry(models.Model):
    TYPE = (
        ('0', True),
        ('1', False)
    )
    id = models.UUIDField(primary_key=True, auto_created=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(to = "auth_user.User", verbose_name = "用户", related_name= "user",on_delete= models.CASCADE)
    if_cough = models.CharField(choices=TYPE,null=True,default="0", max_length=1, verbose_name="是否咳嗽")
    temperature = models.CharField(max_length = 5, null=True, blank=True, verbose_name="温度")
    breath_short = models.CharField(choices=TYPE,null=True,default="0", max_length=1, verbose_name="是否呼吸困难")
    other = models.CharField(max_length=50,null=True, blank=True, verbose_name="其他")
    opdate = models.DateTimeField(default=datetime.now, verbose_name="操作时间")
    unit = models.ForeignKey(Unit, verbose_name = "用户", null=True, blank=True, on_delete= models.CASCADE)

    class Meta:
        verbose_name = "每日申报"
        verbose_name_plural = verbose_name
        db_table = "daily_entry"


class Provinces(models.Model):
    id = models.UUIDField(primary_key=True, auto_created=True, default=uuid.uuid4, editable=False)
    content = models.TextField(null=True, blank=True, verbose_name="内容")
    opdate = models.DateTimeField(default=datetime.now, verbose_name="操作时间")
    class Meta:
        verbose_name = "各省数据"
        verbose_name_plural = verbose_name
        db_table = "provinces"

class News(models.Model):
    id = models.UUIDField(primary_key=True, auto_created=True, default=uuid.uuid4, editable=False)
    content = models.TextField(null=True, blank=True, verbose_name="内容")
    opdate = models.DateTimeField(default=datetime.now, verbose_name="操作时间")
    class Meta:
        verbose_name = "新闻数据"
        verbose_name_plural = verbose_name
        db_table = "news"