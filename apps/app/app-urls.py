from django.conf.urls import url
from django.urls import include
from rest_framework.routers import DefaultRouter
from apps.app.views.views import UnitViewSet, DailyEntryViewSet, ProvincesViewSet, NewsViewSet

router = DefaultRouter()

router.register(r'unit', UnitViewSet)
router.register(r'daily-entry', DailyEntryViewSet)
router.register(r'provinces', ProvincesViewSet)
router.register(r'news', NewsViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
]