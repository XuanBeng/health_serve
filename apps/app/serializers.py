from rest_framework import serializers

from apps import auth_user
from apps.app.models import Unit, DailyEntry, Provinces, News


# from apps.auth_user.serializers import UserDetailSerializer


class UnitSerializer(serializers.ModelSerializer):

    class Meta:
        model = Unit
        fields = "__all__"


class DailyEntrySeializer(serializers.ModelSerializer):

    # user = UserDetailSerializer(read_only=True)
    # user_id = serializers.CharField(write_only=True)

    class Meta:
        model = DailyEntry
        # fields = ("id", "if_cough", "temperature", "breath_short", "other", "opdate", "user_id", "unit_id")
        fields = "__all__"

class ProvincesSeializer(serializers.ModelSerializer):

    class Meta:
        model = Provinces
        fields = "__all__"

class NewsSeializer(serializers.ModelSerializer):

    class Meta:
        model = News
        fields = "__all__"
