import django_filters

from apps.app.models import DailyEntry, Unit


class DailyEntryFilter(django_filters.rest_framework.FilterSet):
    """
    每日申报过滤类
    """
    user = django_filters.CharFilter(field_name="user")
    unit = django_filters.CharFilter(field_name="unit")
    start_date = django_filters.DateTimeFilter(field_name='opdate', lookup_expr='gte')
    end_date = django_filters.DateTimeFilter(field_name='opdate', lookup_expr='lte')

    class Meta:
        model = DailyEntry
        fields = ['user', 'start_date', 'end_date', 'unit']

class UnitFilter(django_filters.rest_framework.FilterSet):
    class Meta:
        model = Unit
        fields = "__all__"