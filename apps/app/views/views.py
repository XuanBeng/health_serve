from django.shortcuts import render

# Create your views here.
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters

from apps.app import serializers
from apps.app.filters import DailyEntryFilter, UnitFilter
from apps.app.models import Unit, DailyEntry, Provinces, News
from apps.app.serializers import UnitSerializer
from apps.auth_user.models import User
from utils.modelViewSet import modelViewSet


class UnitViewSet(modelViewSet):

    queryset = Unit.objects.all()
    serializer_class = serializers.UnitSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_class = UnitFilter



class DailyEntryViewSet(modelViewSet):
    queryset = DailyEntry.objects.all().filter()
    serializer_class = serializers.DailyEntrySeializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_class = DailyEntryFilter

    # search_fields = ['user']  # SearchFilter: like模糊搜索, query params为"search"，与search_fields配合使用
    # ordering_fields = ['user']

class ProvincesViewSet(modelViewSet):

    queryset = Provinces.objects.all()
    serializer_class = serializers.ProvincesSeializer


class NewsViewSet(modelViewSet):

    queryset = News.objects.all()
    serializer_class = serializers.NewsSeializer



