import uuid
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.
from apps.app.models import Unit


# class UserManager(BaseUserManager):
#     def create_user(self,username, name=None, password=None, student_no=None,
#                     mobile=None, relatives_mobile= None ,area=None, type = None, unit =None):
#         """
#         Creates and saves a User with the given email, date of
#         birth and password.
#
#         """
#         if not username:
#             raise ValueError('The given username must be set')
#         user = self.model(username=username,name =name ,student_no=student_no, mobile=mobile, area=area,
#                           relatives_mobile= relatives_mobile , type = type, unit =unit)
#         user.set_password(password)
#         user.save(using=self._db)
#         return user

class User(AbstractUser):
    TYPE = (
        ('0', '普通'),
        ('1', '管理')
    )
    name = models.CharField(max_length=10, verbose_name="用户名")
    student_no = models.CharField(null=True, blank=False, max_length=15, verbose_name="学号")
    mobile = models.CharField(null=True, blank=True, max_length=15, verbose_name="电话")
    relatives_mobile = models.CharField(null=True, blank=True, max_length=15, verbose_name="亲人电话")
    area = models.CharField(null=True, blank=True, max_length=100, verbose_name="地区")
    type = models.CharField(choices=TYPE,null=True, max_length=1, verbose_name="级别")
    weChat_id = models.CharField(null=True, blank=True, max_length=100, verbose_name="微信识别")
    unit = models.ForeignKey(Unit, null=True,verbose_name="所属单元", on_delete=models.CASCADE)



    class Meta:
        verbose_name = "用户信息表"
        verbose_name_plural = verbose_name
        db_table = "user"
