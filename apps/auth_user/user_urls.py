from django.conf.urls import url
from django.urls import include
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token

# from apps.auth_user import views
from rest_framework.authtoken import views
from apps.auth_user.views import UserViewset, CustomAuthToken

router = DefaultRouter()

router.register(r'user', UserViewset)


urlpatterns = [
    # path('admin/', admin.site.urls),
    # url(r'^register/$', views.RegisterView.as_view()),
    url(r'^', include(router.urls)),

    # url(r'^login/', views.obtain_auth_token),#drf自带token
    url(r'^login/', CustomAuthToken.as_view())
]