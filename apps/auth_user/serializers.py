from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from apps.app.serializers import UnitSerializer
from apps.auth_user.models import User


class UserRegSerializer(serializers.ModelSerializer):
    username = serializers.CharField(label="用户名", help_text="用户名", required=True, allow_blank=False,
                                     validators=[UniqueValidator(queryset=User.objects.all(), message="用户已经存在")])
    password = serializers.CharField(
        style={'input_type': 'password'},help_text="密码", label="密码", write_only=True,
    )


    class Meta:
        model = User
        fields = "__all__"
        # fields = ("username", "password", "student_no", "mobile", "unit", "name")
        # extra_kwargs = {'username': {'required': False}}
    def create(self, validated_data):
        user = super(UserRegSerializer, self).create(validated_data=validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user


class UserDetailSerializer(serializers.ModelSerializer):
    """
    用户详情序列化类
    """

    unit = UnitSerializer(read_only=True)
    unit_id = serializers.CharField(write_only=True)

    class Meta:

        model = User
        # fields = ("name", "gender", "birthday", "email", "mobile")
        fields = ("id", "username", "name", "student_no", "mobile", "area" , "type", "unit", "unit_id")
        # fields = "__all__"