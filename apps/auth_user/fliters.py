import django_filters

from apps.auth_user.models import User


class UserFilter(django_filters.rest_framework.FilterSet):

    class Meta:
        model = User
        fields = ("id", "username", "name", "student_no", "mobile", "area" , "type", "unit", "unit_id")