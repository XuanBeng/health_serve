

# Create your views here.


from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters

from utils.modelViewSet import modelViewSet
from .fliters import UserFilter
from .models import User
from .serializers import UserRegSerializer, UserDetailSerializer


class UserViewset(modelViewSet):
    """
    用户
    """
    queryset = User.objects.all()
    # serializer_class = UserRegSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_class = UserFilter
    # def create(self, request, *args, **kwargs):
    #     try:
    #         data = request.data["user"]
    #         # user = {
    #         #     "username": data.username,
    #         #     "password": data.password,
    #         #     "student_no": data.student_no,
    #         #     "name": data.name,
    #         #     "mobile": data.mobile,
    #         #     "unit": data.unit
    #         # }
    #         User.objects.create(**data)
    #         return Response("新增成功")
    #     except Exception as e:
    #         return Response(e)

    def get_serializer_class(self):
        if self.action == "retrieve" or self.action == "list":
            return UserDetailSerializer
        elif self.action == "create":
            return UserRegSerializer
        return UserDetailSerializer




from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response

class CustomAuthToken(ObtainAuthToken):
    # for user in User.objects.all():
    #     Token.objects.get_or_create(user=user)

    def post(self, request, *args, **kwargs):
        # for user in User.objects.all():
        #     Token.objects.get_or_create(user=user)
        try:
            serializer = self.serializer_class(data=request.data,
                                              context={'request': request})
            serializer.is_valid(raise_exception=True)
            user = serializer.validated_data['user']
            token, created = Token.objects.get_or_create(user=user)
            data = {
                'token': token.key,
                'id': user.pk,
                'username': user.username,
                'type': user.type
            }
            return Response({'errCode': 0, 'errMessage': '登录成功', 'data': data})
        except Exception as e:
            return Response({'errCode': 0, 'errMessage': '登录失败', 'data': e})


# class CustomBackend(ModelBackend):
#     """
#     自定义用户验证
#     """
#     def authenticate(username=None, password=None, **kwargs):
#         try:
#             dbUser = get_user_model()
#             user = dbUser.objects.get(Q(username=username)|Q(mobile=username))
#             if user.check_password(password):
#                 return user
#         except Exception as e:
#             return None