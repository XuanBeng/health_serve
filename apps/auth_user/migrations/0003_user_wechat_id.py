# Generated by Django 3.0.3 on 2020-02-09 05:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auth_user', '0002_auto_20200209_1320'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='weChat_id',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='微信识别'),
        ),
    ]
